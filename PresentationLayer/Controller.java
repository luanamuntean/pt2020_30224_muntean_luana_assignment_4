package PresentationLayer;

public class Controller {

private WaiterGraphicalUserInterface waiterGraphicalUserInterface;
private AdministratorGraphicalUserInterface administratorGraphicalUserInterface;
	
	public Controller(WaiterGraphicalUserInterface v){
		this.waiterGraphicalUserInterface = v;
		waiterGraphicalUserInterface.listen();
	}
	
	public Controller(AdministratorGraphicalUserInterface v){
		this.administratorGraphicalUserInterface = v;
		administratorGraphicalUserInterface.listen();
	}
	
}
