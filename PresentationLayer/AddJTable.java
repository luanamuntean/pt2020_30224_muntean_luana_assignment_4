package PresentationLayer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AddJTable implements ActionListener {

private AdministratorGraphicalUserInterface administratorGui;

	
	public AddJTable (AdministratorGraphicalUserInterface administratorGui) {
		super();
		this.administratorGui = administratorGui;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub

		administratorGui.createJTable();
		
	}

}
