package PresentationLayer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import javax.swing.JTextField;

import BusinessLayer.MenuItem;
import BusinessLayer.Order;
import BusinessLayer.Restaurant;

public class viewAllOrders implements ActionListener {

	  private JTextField fieldOrderId;


		public viewAllOrders(JTextField fieldOrderId) {
			super();
			this.fieldOrderId = fieldOrderId;
		}


	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
		try {
		int orderId=Integer.parseInt(fieldOrderId.getText());
		Iterator iterator=Restaurant.orders.entrySet().iterator();
		
		while(iterator.hasNext())
		{
			Map.Entry<Order,ArrayList<MenuItem>>ord=(Map.Entry<Order, ArrayList<MenuItem>>)iterator.next();
			if(ord.getKey().getOrderId() == orderId)
			{
				generateAllOrders(orderId);
			}
		}
	}catch (Exception e1) {
		System.out.println(e1.getMessage());
	}
}

	
	
	public float computePrice(Order order) {
		float totalPrice=0.0f;
		ArrayList<MenuItem>objects=Restaurant.orders.get(order);
		for(MenuItem obj:objects) {
			totalPrice=totalPrice+obj.getPrice();
		}
	
		return totalPrice;
	}

	private void generateAllOrders(int orderId) {
		// TODO Auto-generated method stub
		Iterator iterator=Restaurant.orders.entrySet().iterator();
		
		while(iterator.hasNext()){
			Map.Entry<Order, ArrayList<MenuItem>>ord=(Map.Entry<Order, ArrayList<MenuItem>>)iterator.next();
			
			if(ord.getKey().getOrderId()==orderId) {
				for(MenuItem item:ord.getValue()) {
					fieldOrderId.add(null, item.toString());
				}
				float totalPrice=computePrice(ord.getKey());
				
			}
		}
		
	}

}
