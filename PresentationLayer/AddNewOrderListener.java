package PresentationLayer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import BusinessLayer.Restaurant;

public class AddNewOrderListener implements ActionListener {

	private WaiterGraphicalUserInterface waiterGui;

	
	
	public AddNewOrderListener(WaiterGraphicalUserInterface waiterGui) {
		super();
		this.waiterGui = waiterGui;
	}



	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		Restaurant rest=waiterGui.getRestaurant();
		
		String idS1 =waiterGui.getOrderId().getText(); 
		String tableS2 =waiterGui.getTableNr().getText(); 
		String nameS3 =waiterGui.getNamesProducts().getText(); 

		
		
    int id=Integer.parseInt(idS1);
    int tableNr=Integer.parseInt(tableS2);

	rest.createNewOrder(id,tableNr,nameS3);
    System.out.println(rest.createOrdersString());     



	}

}
