package PresentationLayer;

import java.awt.event.ActionEvent;

import java.awt.event.ActionListener;

import BusinessLayer.Restaurant;

public class DeleteItemFromMenuListener implements ActionListener {

	
private AdministratorGraphicalUserInterface administratorGui;

	
	public DeleteItemFromMenuListener(AdministratorGraphicalUserInterface administratorGui) {
		super();
		this.administratorGui = administratorGui;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		Restaurant rest=administratorGui.getRestaurant();
		String productS1 =administratorGui.getCerinta1().getText(); 
		rest.deleteMenuItem(productS1);
        System.out.println(rest.createMenuListString());


	}

	

}
