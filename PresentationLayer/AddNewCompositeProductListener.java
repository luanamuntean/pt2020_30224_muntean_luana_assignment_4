package PresentationLayer;

import java.awt.event.ActionEvent;

import java.awt.event.ActionListener;
import java.util.ArrayList;

import BusinessLayer.MenuItem;
import BusinessLayer.Restaurant;

public class AddNewCompositeProductListener implements ActionListener {

private AdministratorGraphicalUserInterface administratorGui;

	
	public AddNewCompositeProductListener(AdministratorGraphicalUserInterface administratorGui) {
		super();
		this.administratorGui = administratorGui;
	}
	 
	
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
		Restaurant rest=administratorGui.getRestaurant();

		
		String productS1 =administratorGui.getCerinta1().getText(); 
		String productS2 =administratorGui.getCerinta5().getText(); 
		
		System.out.println(rest.createNewCompositeProduct( productS1,productS2));
        System.out.println(rest.createMenuListString());


	}




	
	
}
