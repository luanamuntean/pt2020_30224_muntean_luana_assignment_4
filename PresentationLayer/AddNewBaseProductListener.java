package PresentationLayer;

import java.awt.event.ActionEvent;


import java.awt.event.ActionListener;

import BusinessLayer.Restaurant;

public class AddNewBaseProductListener implements ActionListener {

	private AdministratorGraphicalUserInterface administratorGui;

	
	public AddNewBaseProductListener(AdministratorGraphicalUserInterface administratorGui) {
		super();
		this.administratorGui = administratorGui;
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub

		Restaurant rest=administratorGui.getRestaurant();
		
		String productS1 =administratorGui.getCerinta1().getText(); 
		String productS2 =administratorGui.getCerinta3().getText(); 
		
		float productPrice=Float.parseFloat(productS2);

		System.out.println(rest.createNewBaseProduct(0,productS1,productPrice));
		
		
        System.out.println(rest.createMenuListString());


	}

	

}
