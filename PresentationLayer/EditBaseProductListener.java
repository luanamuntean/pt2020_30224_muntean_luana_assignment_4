package PresentationLayer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedList;

import javax.swing.JTextField;

import BusinessLayer.BaseProduct;
import BusinessLayer.CompositeProduct;
import BusinessLayer.MenuItem;
import BusinessLayer.Restaurant;

public class EditBaseProductListener implements ActionListener {

private AdministratorGraphicalUserInterface administratorGui;

	
	public EditBaseProductListener(AdministratorGraphicalUserInterface administratorGui) {
		super();
		this.administratorGui = administratorGui;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		Restaurant rest=administratorGui.getRestaurant();
		
		String productS1 =administratorGui.getCerinta1().getText(); 
		String productS2 =administratorGui.getCerinta2().getText();
		String productS3 =administratorGui.getCerinta3().getText(); 
		String productS4 =administratorGui.getCerinta4().getText();
		
		float productPrice=Float.parseFloat(productS3);
		float productNewPrice=Float.parseFloat(productS4);


		
		rest.editBaseProduct(productS1,productS2,productPrice,productNewPrice);
		
        System.out.println(rest.createMenuListString());



	}



}
