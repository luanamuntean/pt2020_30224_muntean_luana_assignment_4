 package PresentationLayer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

import BusinessLayer.BaseProduct;
import BusinessLayer.CompositeProduct;
import BusinessLayer.MenuItem;
import BusinessLayer.Restaurant;

public class MainClass {
	
    public static void main(String args[]) {
    	
    	Restaurant restaurant = new Restaurant();
    	restaurant.setMenu(createInitMenu());
        System.out.println(restaurant.createMenuListString());

    	
    	WaiterGraphicalUserInterface view1 = new WaiterGraphicalUserInterface("Waiter View",restaurant);
    	AdministratorGraphicalUserInterface view2 = new AdministratorGraphicalUserInterface("Administrator View",restaurant);

        Controller controller1 = new Controller(view1);
        Controller controller2 = new Controller(view2);
             

}
    
    
    
    private static HashMap<String, MenuItem> createInitMenu(){
        HashMap<String, MenuItem>  menu = new HashMap<>();

        MenuItem eggplant = new BaseProduct("Eggplant", 2);
        MenuItem zucchini = new BaseProduct("Zucchini",(float) 1.05);
        MenuItem tomatoes = new BaseProduct("Tomatoes", (float) 1.5);
        // ----
        MenuItem pineapple = new BaseProduct("Pineapple", (float) 3.2);
        MenuItem strawberries = new BaseProduct("Strawberries", (float) 3.05);
        MenuItem lime = new BaseProduct("Lime", (float) 1.75);

        ArrayList<MenuItem> subComp1 = new ArrayList<>();
        subComp1.add(eggplant);
        subComp1.add(zucchini);
        subComp1.add(tomatoes);
        MenuItem grilledVeggies = new CompositeProduct("Grilled_veggies",subComp1);


        ArrayList<MenuItem> subComp2 = new ArrayList<>();
        subComp2.add(pineapple);
        subComp2.add(strawberries);
        subComp2.add(lime);
        MenuItem fruitSalad = new CompositeProduct("Fruit_Salad", subComp2);
 
        menu.put("Eggplant",eggplant);
        menu.put("Zucchini",zucchini);
        menu.put("Tomatoes",tomatoes);
        menu.put("Grilled_Veggies", grilledVeggies);

        menu.put("Pineapple",pineapple);
        menu.put("Strawberries",strawberries);
        menu.put("Lime",lime);
        menu.put("Fruit_Salad", fruitSalad);


        return menu;
    }
    
    
    
   
}