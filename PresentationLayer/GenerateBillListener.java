package PresentationLayer;

import java.awt.event.ActionEvent;


import java.awt.event.ActionListener;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

import javax.swing.JOptionPane;
import javax.swing.JTextField;

import BusinessLayer.MenuItem;
import BusinessLayer.Order;
import BusinessLayer.Restaurant;

public class GenerateBillListener implements ActionListener {

	  private WaiterGraphicalUserInterface waiterGui;
	  Restaurant rest=new Restaurant();



	public GenerateBillListener(WaiterGraphicalUserInterface waiterGui) {
		super();
		this.waiterGui = waiterGui;
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
		try {
			
		 int orderId=Integer.parseInt(waiterGui.getOrderId().getText());
		// System.out.println(fieldOrderId.getText());
		Order order=null;
		
		Iterator iterator = Restaurant.orders.entrySet().iterator(); 
		
        while (iterator.hasNext()) 
        {
        	 Map.Entry<Order, ArrayList<MenuItem>> ord =(Map.Entry<Order, ArrayList<MenuItem>> ) iterator.next();
        	 if(ord.getKey().getOrderId() == orderId) {
        		 order=ord.getKey();
        	 }
        }
        
        if(order!=null) 
        	rest.generateBill(order);
		else
			JOptionPane.showMessageDialog(null, "Order not found");
	} catch (Exception e1) {
		System.out.println(e1.getMessage());
	}

	}


	
	

}
