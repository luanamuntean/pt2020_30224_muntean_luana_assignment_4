package PresentationLayer;
import javax.swing.*;


import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import BusinessLayer.BaseProduct;
import BusinessLayer.CompositeProduct;
import BusinessLayer.MenuItem;
import BusinessLayer.Order;
import BusinessLayer.Restaurant;
import BusinessLayer.RestaurantProcessing;



public class WaiterGraphicalUserInterface extends JFrame{
	private JPanel panel = new JPanel();
	private JButton buton1 = new JButton("Add Product to Order");
	private JButton buton2 = new JButton("View All Orders");
	private JButton buton3 = new JButton("Generate Bill");
	private JButton buton4 = new JButton("Add new Order");


	private JLabel label1 = new JLabel("Order Id:");
	private JLabel label2 = new JLabel("Table Number:");
	private JLabel label3 = new JLabel("Names for products to add to order:");
	private JTextField orderId = new JTextField(30);
	private JTextField tableNr = new JTextField(30); //pt ca vrem sa scriem in ele
	private JTextField namesProducts = new JTextField(30);
	
	
	
	private Restaurant restaurant;
	
	
	
	public Restaurant getRestaurant() {
		return restaurant;
	}


	public void setRestaurant(Restaurant restaurant) {
		this.restaurant = restaurant;
	}
	
	public WaiterGraphicalUserInterface(String name,Restaurant restaurant) {
		super(name);
		
		this.restaurant=restaurant;

	   
		
		JPanel P1 = new JPanel();
		JPanel P2 = new JPanel();
		JPanel P3 = new JPanel();
		JPanel P4 = new JPanel();
		
		P1.add(label1);
		P1.add(orderId);
		
		P2.add(label2);
		P2.add(tableNr);
		
		P3.add(label3);
		P3.add(namesProducts);
		
		P4.add(buton1);
		P4.add(buton2);
		P4.add(buton3);
		P4.add(buton4);

		
		
		
		
		panel.add(P1);
		panel.add(P2);
		panel.add(P3);
		panel.add(P4);
		
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS)); 
		
		this.add(panel);


		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.pack();
		this.setVisible(true);
	}
	
	public JTextField getOrderId() {
		return orderId;
	}
	
	public JTextField getTableNr() {
		return tableNr;
	}
	
	public  JTextField getNamesProducts() {
		return namesProducts;
	}
	
	public void setNamesProducts(String idsProducts) {
		this.namesProducts.setText(idsProducts);
	}
	
	
	
	public JButton getButonAddProduct() {
		return buton1;
	}
	
	public JButton getButonGenerateOrder() {
		return buton2;
	}
	
	public JButton getButonGenerateBill() {
		return buton3;
	}
	
	

	public void listen(){
		buton1.addActionListener(new AddProductListener());
		
		
		buton2.addActionListener(new viewAllOrders(orderId));
		buton3.addActionListener(new GenerateBillListener(this));
		buton4.addActionListener(new AddNewOrderListener(this));
		

	}


	
	

	

}