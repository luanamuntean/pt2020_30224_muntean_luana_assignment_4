package PresentationLayer;
import javax.swing.*;



import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import BusinessLayer.BaseProduct;
import BusinessLayer.CompositeProduct;
import BusinessLayer.MenuItem;
import BusinessLayer.Order;
import BusinessLayer.Restaurant;
import BusinessLayer.RestaurantProcessing;



public class AdministratorGraphicalUserInterface extends JFrame{
	private JPanel panel = new JPanel();
	private JButton butonAddBP = new JButton("Add new Base Product");
	private JButton butonAddCP = new JButton("Add new Composite Product");
	private JButton butonEditBP = new JButton("Edit Base Product");
	private JButton butonEditCP = new JButton("Edit Composite Product");
	private JButton butonDeleteItem = new JButton("Delete Item From Menu");
	private JButton butonJTable = new JButton("JTable");

	
	
	private JLabel label1 = new JLabel("Product name:");	
	private JLabel label2 = new JLabel("New product name:");
	private JLabel label3 = new JLabel("Product price:");
	private JLabel label4 = new JLabel("New product price:");
	private JLabel label5 = new JLabel("Product subcomponents:");
	private JLabel label6 = new JLabel("New product subcomponents:");

	private JTextField cerinta1 = new JTextField(30); //pt ca vrem sa scriem in ele
	private JTextField cerinta2 = new JTextField(30);
	private JTextField cerinta3 = new JTextField(30);
	private JTextField cerinta4 = new JTextField(30); 
	private JTextField cerinta5 = new JTextField(30);
	private JTextField cerinta6 = new JTextField(30);
	
	
	
	private Restaurant restaurant;
	
	
	
	public Restaurant getRestaurant() {
		return restaurant;
	}


	public void setRestaurant(Restaurant restaurant) {
		this.restaurant = restaurant;
	}


	public AdministratorGraphicalUserInterface(String name,Restaurant restaurant) {
		super(name);
		
		this.restaurant=restaurant;

	   
		JPanel P1 = new JPanel();		
		JPanel P2 = new JPanel();
		JPanel P3 = new JPanel();
		JPanel P4 = new JPanel();
		JPanel P5 = new JPanel();
		JPanel P6 = new JPanel();
		JPanel P7 = new JPanel();

	
		P1.add(label1);
		P1.add(cerinta1);
		
		P2.add(label2);
		P2.add(cerinta2);
		
		P3.add(label3);
		P3.add(cerinta3);
		
		
		P4.add(label4);
		P4.add(cerinta4);
		
		P5.add(label5);
		P5.add(cerinta5);
		
		P6.add(label6);
		P6.add(cerinta6);
		
		P7.add(butonAddBP);
		P7.add(butonAddCP);
		P7.add(butonEditBP);
		P7.add(butonEditCP);
		P7.add(butonDeleteItem);
		P7.add(butonJTable);
		

		panel.add(P1);
		panel.add(P2);
		panel.add(P3);
		panel.add(P4);
		panel.add(P5);
		panel.add(P6);
		panel.add(P7);

		
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS)); 
		
		this.add(panel);


		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.pack();
		this.setVisible(true);
	}
	
	
	public JTextField getCerinta1() {
		return cerinta1;
	}
	
	public JTextField getCerinta2() {
		return cerinta2;
	}
	
	public  JTextField getCerinta3() {
		return cerinta3;
	}
	
	public  JTextField getCerinta4() {
		return cerinta4;
	}
	
	
	public JTextField getCerinta5() {
		return cerinta5;
	}
	
	public JTextField getCerinta6() {
		return cerinta6;
	}
	
	
	
	public JButton getButonAddNewBaseProduct() {
		return butonAddBP;
	}
	
	public JButton getButonAddNewCompositeProduct() {
		return butonAddCP;
	}
	
	public JButton getButonEditBaseProduct() {
		return butonEditBP;
	}
	
	public JButton getButonEditCompositeProduct() {
		return butonEditCP;
	}
	
	
	public JButton getButonDeleteItemFromMenu() {
		return butonDeleteItem;
	}
	
	public JButton getButonJTable() {
		return butonJTable;
	}
	

	public void listen(){
		butonAddBP.addActionListener(new AddNewBaseProductListener(this));
		
		butonAddCP.addActionListener(new AddNewCompositeProductListener(this));
		butonEditBP.addActionListener(new EditBaseProductListener(this));
		butonEditCP.addActionListener(new EditCompositeProductListener(this));
		butonDeleteItem.addActionListener(new DeleteItemFromMenuListener(this));
		butonJTable.addActionListener(new AddJTable(this));
		
		

	}
	
	public void createJTable() {
		
		JFrame nameMenu=new JFrame();
		
		String[] columnNames = {"Name Product",
                "Ingredients",
                "Price"};
	
	
	Object[][] data = {
		    {"Kiwi", "-","5"},
		    {"Raw-Salad", "Tomatoes;Eggplant","15"}
		 
		};
	
	JTable table = new JTable(data, columnNames);

	JScrollPane scrollPane = new JScrollPane(table);
	table.setFillsViewportHeight(true);
	nameMenu.add(scrollPane);
	
	


	nameMenu.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	nameMenu.pack();
	nameMenu.setVisible(true);
	
}
}