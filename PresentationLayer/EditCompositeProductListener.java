package PresentationLayer;

import java.awt.event.ActionEvent;

import java.awt.event.ActionListener;

import BusinessLayer.Restaurant;

public class EditCompositeProductListener implements ActionListener {

	private AdministratorGraphicalUserInterface administratorGui;

	
	public EditCompositeProductListener(AdministratorGraphicalUserInterface administratorGui) {
		super();
		this.administratorGui = administratorGui;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
		Restaurant rest=administratorGui.getRestaurant();
		
		String productS1 =administratorGui.getCerinta1().getText(); 
		String productS2 =administratorGui.getCerinta2().getText(); 
		String productS3 =administratorGui.getCerinta6().getText(); 
		
		rest.editCompositeProduct(productS1, productS2,productS3);
        System.out.println(rest.createMenuListString());


	}
	

}
