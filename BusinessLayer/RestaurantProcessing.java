package BusinessLayer;

import java.util.ArrayList;
import java.util.Date;

public interface RestaurantProcessing {
 
	public BaseProduct createNewBaseProduct(int id, String name, float price);
	public CompositeProduct createNewCompositeProduct(String name,String compositeProductsName);
	
	public void deleteMenuItem(String nameProductDel);
	
	public void editBaseProduct(String name, String newName, float price,float newPrice);
	public void editCompositeProduct( String name,String newNameCP,String compositeProductsName);
	
	public Order createNewOrder(int orderId,int nrTable,String productsName); 
	
	public float computePrice(Order order);
	
	public void generateBill(Order order);
	


}
