package BusinessLayer;

import java.awt.Component;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.util.ArrayList;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Observable;


public class Restaurant extends Observable implements RestaurantProcessing {

	public static HashMap<Order, ArrayList<MenuItem>> orders;
	public static HashMap<String, MenuItem>  menu = new HashMap<>();
	
	
	 
	
	
	public int currentId=0;

	public Restaurant() {
		Restaurant.orders = new HashMap<Order, ArrayList<MenuItem>>();
	}

	public HashMap<Order, ArrayList<MenuItem>> getOrders() {
		return orders;
	}

	public void setOrders(HashMap<Order, ArrayList<MenuItem>> orders) {
		Restaurant.orders = orders;
	}

	

	public HashMap<String, MenuItem> getMenu() {
		return menu;
	}

	public void setMenu(HashMap<String, MenuItem> menu) {
		Restaurant.menu = menu;
	}

	
	
	
	/**
	 * @param id reprezinta id-ul produsului pe care il cream
	 * @param nameBP reprezinta numele produsului pe care il cream
	 * @param priceBP reprezinta pretul produsului pe care il cream

	 * @return un produs de tipul BaseProduct
	 */
	
	
	@Override
	public BaseProduct createNewBaseProduct(int id, String nameBP, float priceBP) {
		// TODO Auto-generated method stub
	
		
		HashMap<String, MenuItem> objectsMenu=Restaurant.menu;
		
		BaseProduct baseProduct=null;
		
		
		if(!objectsMenu.containsKey(nameBP) && !nameBP.isEmpty())
		{
			baseProduct=new BaseProduct(currentId++, nameBP, priceBP);
			objectsMenu.put(nameBP, baseProduct);
		}    	   
		
		
		return baseProduct;
		
	}

	
	/**
	 * @param nameCP reprezinta numele produsului compus pe care il cream
	 * @param compositeProductsName reprezinta string-ul de produse  de baza din care este alcatuit produsul compus

	 * @return un produs de tipul CompositeProduct
	 */
	
	
	@Override
	public CompositeProduct createNewCompositeProduct( String nameCP, String compositeProductsName) {
		// TODO Auto-generated method stub
		
		
		HashMap<String, MenuItem> menu=Restaurant.menu;
		
		
		CompositeProduct compositeProduct = null;
		
			
			if(!nameCP.isEmpty()) {
				currentId++;
				
				String [] productsStringArray=compositeProductsName.split(";");
				
				ArrayList<MenuItem>newProducts=new ArrayList<>();
				
				for(String productName:productsStringArray) {
				    String nameBP=productName; //eggplant
				   
				     if(menu.containsKey(nameBP))
				     {
				    	   MenuItem menuItem= menu.get(nameBP);
				    	   newProducts.add(menuItem);
				    		
				     }
				     
				   
				   
				}
				compositeProduct=new CompositeProduct(currentId,nameCP,newProducts);
			
                menu.put(nameCP, compositeProduct);
             
				
			}
			
		
		

		
		return compositeProduct;
		
		
	}

	
	
	/**

	 * @param nameProductDel reprezinta numele produsului pe care urmeaza sa il stergem

	 *
	 */
	
	@Override
	public void deleteMenuItem(String nameProductDel) {
		// TODO Auto-generated method stub
		
		HashMap<String, MenuItem> objectsMenu=Restaurant.menu;
	
      	 
      	  if(objectsMenu.containsKey(nameProductDel))
      	  
      		 objectsMenu.remove(nameProductDel) ;
      		 else
      			 System.out.println("This product doesn't exist in this menu");
      	 
      		 
	
	}

	
	
	/**
	
	 * @param nameBP reprezinta numele produsului pe care vrem sa il editam
	  * @param newNameBP reprezinta numele noului produs
	 * @param priceBP reprezinta pretul produsului pe care vrem sa il editam
	 * @param newPriceBP reprezinta pretul noului produs


	 * 
	 */
	
	@Override
	public void editBaseProduct(String nameBP,String newNameBP,float priceBP, float newPriceBP) {
		// TODO Auto-generated method stub
	 
		HashMap<String, MenuItem> objectsMenu=Restaurant.menu;

   		
		BaseProduct bp=null;
			
			if(objectsMenu.containsKey(nameBP))
			{
				if(nameBP.equals(newNameBP))
					{
					   bp=new BaseProduct(currentId,nameBP,newPriceBP);
					   objectsMenu.put(nameBP,bp);
					}
				else
				{
				objectsMenu.remove(nameBP);
				 bp=new BaseProduct(currentId, newNameBP, newPriceBP);
				objectsMenu.put(newNameBP,bp);
			}
			}
			
					

		
	}
	
	
/**
	
	 * @param nameCP reprezinta numele produsului compus pe care vrem sa il editam
	  * @param newNameCP reprezinta numele noului produs
	 * @param compositeProductsName string-ul produselor simple din care este alcatuit produsul compus
	
	 * 
	 */
	

	@Override
	public void editCompositeProduct(String nameCP, String newNameCP, String compositeProductsName) {
		// TODO Auto-generated method stub

		
		HashMap<String, MenuItem> objectsMenu=Restaurant.menu;
		
		CompositeProduct compositeProduct = null;

		
		
          
          
 
          if(objectsMenu.containsKey(nameCP))
          {
      		System.out.println("IF");

        	
        	  String [] productsStringArray=compositeProductsName.split(";");
				
				ArrayList<MenuItem>newProducts=new ArrayList<>();
				
				for(String productName:productsStringArray) {
				   String nameBP=productName; 
				   
				   System.out.println("For");
				   
				     if(menu.containsKey(nameBP))
				     {
				    	   MenuItem menuItem= menu.get(nameBP);
				    	   newProducts.add(menuItem);
				    		
				     }
				   
				   
				}
				  if(newProducts.isEmpty())
		            	System.out.println("Produse goale");
		            else
		            	{
		            	  for(MenuItem products:newProducts)
		            		  System.out.println(products.getPrice());
		            	}
				
				compositeProduct=new CompositeProduct(currentId,newNameCP,newProducts);
				
				
            	
          
          }
          
          
        	  objectsMenu.remove(nameCP);
        	  objectsMenu.put(newNameCP, compositeProduct);
         
        		  
		
		
	}

	

	@Override
	public float computePrice(Order order) {
		// TODO Auto-generated method stub
		float totalPrice=0.0f;
		ArrayList<MenuItem>objects=Restaurant.orders.get(order);
		for(MenuItem obj:objects) {
			totalPrice=totalPrice+obj.getPrice();
		}
	
		return totalPrice;
	}

	
/**
	
	 * @param order reprezinta comanda pentru care trebuie sa generam un bon fiscal
	 * 
	 */
	
	@Override
	public void generateBill(Order order) {
		// TODO Auto-generated method stub
		
		
		FileWriter fileWriter;
		
		try {
			
			fileWriter=new FileWriter(new File("Orderid" + order.getOrderId() + ".txt"));
			
			
			PrintWriter writer=new PrintWriter(fileWriter);
			ArrayList<MenuItem>objects=Restaurant.orders.get(order);
			writer.println("Order is:" + order.getOrderId());
			writer.println("Table is:" + order.getTable());
			writer.println("Date is:" + order.getDate());
			
			float totalPrice=computePrice(order);
			writer.println("Total price is:" + totalPrice + "$");
			writer.close();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	
	
/**
	
	 * @param orderId reprezinta id-ul comenzii pe care vrem sa o cream
	  * @param nrTable reprezinta numarul mesei pentru care se creeaza o comanda
	 * @param productsName reprezinta produsele pe care clientii doresc sa le comande

	 * 
	 */

	@Override
	public Order createNewOrder(int orderId, int nrTable, String productsName) {
		// TODO Auto-generated method stub

		HashMap<String, MenuItem> menu=Restaurant.menu;

		
		Order newOrder=null;
		
		ArrayList<MenuItem>newProducts=new ArrayList<>();

		if(nrTable>0 && productsName!=null)
		{
			newOrder=new Order(orderId,new Date(System.currentTimeMillis()),nrTable);

			String[] productsStringArray=productsName.split(";");
			for(String pName:productsStringArray) {
				String nameBP=pName;
				
				if(menu.containsKey(nameBP)) {
					MenuItem menuItem=menu.get(nameBP);
					newProducts.add(menuItem);
				}
				
			}
			
			
			
			Restaurant.orders.put(newOrder, newProducts);
			
			
		}
		
		
		return newOrder;
	}
	
	
	
	
	
	
	
	public boolean productIsWellFormed() {
		int item=0;
		  for(HashMap.Entry< String, MenuItem> entry : menu.entrySet()) {
			if(entry.getValue().getPrice()>0.0f && entry.getValue().getId()>0  &&
					(entry instanceof BaseProduct||entry instanceof CompositeProduct))
				item++;
		}
		
		if(item == Restaurant.menu.size())
			return true;
		else
			return false;
		
	}
	
	
/**
	
	 * @param nameProduct reprezinta numele produsului pe care vrem sa il adaugam la o anumita comanda
	  * @param orderId reprezinta id-ul comenzii la care dorim sa adaugam produse
	 * 
	 */
	
	
	public void addMenuItemInOrder(String nameProduct,int orderId) {
      
		HashMap<Order, ArrayList<MenuItem>> orders=Restaurant.orders;

	    Order order=null;
		
		Iterator iterator = Restaurant.orders.entrySet().iterator(); 
		
        while (iterator.hasNext()) 
        {
        	 Map.Entry<Order, ArrayList<MenuItem>> ord =(Map.Entry<Order, ArrayList<MenuItem>> ) iterator.next();
        	 if(ord.getKey().getOrderId() == orderId) {
        		 order=ord.getKey();
        	 }
        }
        
 	  ArrayList<MenuItem> menuItem= orders.get(nameProduct);
 	  
	   MenuItem product= menu.get(nameProduct);

 	  
 	   menuItem.add(product);

        orders.put(order,menuItem);
	}
	
	
    public String createMenuListString(){
        String menuString = "\n~~ Menu ~~";

        for(HashMap.Entry< String, MenuItem> entry : menu.entrySet()){
            MenuItem m = entry.getValue();
            String priceString =  new DecimalFormat("#.###").format(m.computePrice());
            menuString+= "\n \'"+m.getName()+"\' ---- "+priceString+"$";
        }
        return menuString;
    }
    
    
    public String createOrdersString(){
        String ordersString = "\n   -- ORDERS --";
        for (HashMap.Entry<Order,ArrayList<MenuItem>> entry: orders.entrySet()) {
            Order order = entry.getKey();
            ArrayList<MenuItem> menuItems = entry.getValue();

            String menuChoices ="";
            for(MenuItem m : menuItems){
                menuChoices +="\n* "+m.getName();
            }

            ordersString += "\n OrderID: "+ order.getOrderId() +
                            "\n Table number: "+ order.getTable() +
                            "\n Date: " + order.getDate() +
                            "\n Menu choices: "+menuChoices;
        }
        return ordersString;

    }
    
	
	

}
