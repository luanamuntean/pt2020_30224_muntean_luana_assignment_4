package BusinessLayer;



public class BaseProduct extends MenuItem {

	private int id;
	private String name;
	private float price;
	
	
	
	public BaseProduct(int id, String name, float price) {
		super();
		this.id = id;
		this.name = name;
		this.price = price;
	}
	
	public BaseProduct( String name, float price) {
		super();
		this.name = name;
		this.price = price;
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	
	
	public float computePrice() {
		return price;
	}
	
	public String toString() {
		String res;
		res="Product (" + name + ", price" + price +")";
		return res;
	}
	
	
}
