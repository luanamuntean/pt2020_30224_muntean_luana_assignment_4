package BusinessLayer;

import java.util.Date;

public class Order {
    private int orderId;
    private Date date;
    private int table;
    
    
	public Order(int orderId, Date date, int table) {
		super();
		this.orderId = orderId;
		this.date = date;
		this.table = table;
	}


	public int getOrderId() {
		return orderId;
	}


	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}


	public Date getDate() {
		return date;
	}


	public void setDate(Date date) {
		this.date = date;
	}


	public int getTable() {
		return table;
	}


	public void setTable(int table) {
		this.table = table;
	}
    
	
	@Override
    public int hashCode() {
        return orderId*table + date.hashCode();
    }
	
	
	
	public boolean equals(Object obj) {
	
		Order order = (Order) obj;
		return this.orderId==order.orderId;
	}
	
	
	public String toString() {
		String order;
		order="Order [orderId=" + orderId + ", date=" + date + ", table=" + table + "]";
		return order;
	}
    
    
}
