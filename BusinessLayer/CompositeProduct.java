package BusinessLayer;

import java.util.ArrayList;

public class CompositeProduct extends MenuItem {

	private int id;
	private String name;
	private float price;
	private ArrayList<MenuItem>objects;
	
	public CompositeProduct(int id, String name, ArrayList<MenuItem> objects) {
		super();
		this.id = id;
		this.name = name;
		this.objects=objects;
		
		this.computePrice();
		
	}
	
	

	
	public CompositeProduct(String name, ArrayList<MenuItem> objects) {
		super();
		this.name = name;
		this.objects=objects;
		this.computePrice();
	}
	
	public CompositeProduct(int id, String name) {
		super();
		this.id = id;
		this.name = name;
		this.price = 0.0f;
		this.objects = new ArrayList<MenuItem>();
	}
	
	
	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public float getPrice() {
		return price;
	}


	public void setPrice(float price) {
		this.price = price;
	}


	public ArrayList<MenuItem> getObjects() {
		return objects;
	}

	public void setObjects(ArrayList<MenuItem> objects) {
		this.objects = objects;
	}
	
	
	public float computePrice() {
		float totalPrice=0.0f;
		for(MenuItem obj:objects) {
			totalPrice=totalPrice+obj.getPrice();
		}
		price=totalPrice;
		return price;
	}
	
	public void addMenu(MenuItem menu) {
		objects.add(menu);
		computePrice();
		
	}
	
	@Override
	public String toString() {
		String res;
		res="CompositProduct [id=" + id + ", name=" + name + ", price=" + price + "]";
		
		for(MenuItem items : objects){
			res=res+items.toString()+" ";
		}
		return res;
	}
	
}
