package BusinessLayer;

public class MenuItem {

	private int id;
	private String name;
	private float price;
	


	public int getId() {
		return id;
	}
	
	
	public float getPrice() {
		return price;
	}
	
	public  String getName() {
		return name;
	}
	
	

	public  void setPrice(float price) {
		this.price=price;
	}
	
	public  void setName(String name) {
		this.name=name;
	}

	public float computePrice() {
		return price;
	}

	
}
